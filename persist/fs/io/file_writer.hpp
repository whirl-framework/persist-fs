#pragma once

#include <persist/fs/fs.hpp>

#include <rewrite/writer.hpp>

#include <wheels/support/noncopyable.hpp>

namespace persist::fs {

//////////////////////////////////////////////////////////////////////

// Open -> [Write | Sync] -> Close | ~FileWriter
// TODO: Typestate correctness

class FileWriter : public rewrite::IWriter, private wheels::NonCopyable {
 public:
  FileWriter(fs::Path path);

  // Movable
  FileWriter(FileWriter&&);

  // Non-copyable
  FileWriter(const FileWriter&) = delete;

  ~FileWriter();

  // One-shot
  fallible::Status Open();

  fallible::Status Sync();

  // IWriter

  fallible::Status Write(wheels::ConstMemView data) override;
  fallible::Status Flush() override;

  // Or ~FileWriter
  fallible::Status Close();

 private:
  bool IsValid() const;
  void EnsureValid();

  Fd ReleaseFd();

 private:
  IFileSystem& fs_;
  Path path_;
  Fd fd_ = -1;
};

//////////////////////////////////////////////////////////////////////

// Make and open new writer
fallible::Result<FileWriter> Write(Path path);

}  // namespace persist::fs
