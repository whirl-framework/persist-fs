# Persist-Filesystem

## Contents

- [`fs`](persist/fs) – Filesystem
  - `fs::IFilesystem`
  - `fs::Path`
  - `fs::FileWriter` / `fs::FileReader`
- [`log`](persist/log) – Write-Ahead Logging (WAL)
  - `log::LogWriter` / `log::LogReader` 
- [`matrix`](persist/matrix) – Test runtime

## Examples

- [`Files`](examples/fs/main.cpp)
- [`Log`](examples/log/main.cpp)

## Inspiration

- [Files are hard](https://danluu.com/file-consistency/)
