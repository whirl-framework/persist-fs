#include <persist/matrix/fs/fs.hpp>

#include <timber/log/log.hpp>

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

using persist::fs::Fd;
using persist::fs::FileMode;

using fallible::Result;
using fallible::Status;

namespace persist::fs::matrix {

FileSystem::FileSystem(timber::ILogBackend& log_backend)
    : logger_("Filesystem", log_backend) {
}

// System calls

bool FileSystem::Exists(const fs::Path& file_path) const {
  return files_.find(file_path.Repr()) != files_.end();
}

Result<Fd> FileSystem::Open(const fs::Path& file_path, FileMode mode) {

  auto file = FindOrCreateFile(file_path, mode);
  if (!file) {
    return fallible::Fail(
        fallible::Err(1)
          .Domain("Filesystem")
          .Reason("No such file or directory")
          .Done());
  }
  Fd fd = ++next_fd_;
  size_t offset = InitOffset(file, mode);
  opened_files_.emplace(fd, OpenedFile{fd, file_path, mode, offset, file});
  return fallible::Ok(fd);
}

Result<size_t> FileSystem::Read(Fd fd, wheels::MutableMemView buffer) {
  OpenedFile& of = GetOpenedFile(fd);
  CheckMode(of, FileMode::Read);

  TIMBER_LOG_DEBUG("Read {} bytes from {}", buffer.Size(), of.path);
  size_t bytes_read = of.file->PRead(of.offset, buffer);
  of.offset += bytes_read;
  return fallible::Ok(bytes_read);
}

Status FileSystem::Append(Fd fd, wheels::ConstMemView data) {
  OpenedFile& of = GetOpenedFile(fd);
  CheckMode(of, FileMode::Append);
  TIMBER_LOG_DEBUG("Append {} bytes to {}", data.Size(), of.path);
  of.file->Append(data);
  return fallible::Ok();
}

Status FileSystem::Sync(Fd /*fd*/) {
  return fallible::Ok();
}

Status FileSystem::Close(Fd fd) {
  auto it = opened_files_.find(fd);
  if (it != opened_files_.end()) {
    opened_files_.erase(it);
    return fallible::Ok();
  } else {
    RaiseError("File not found by fd");
  }
}

Result<bool> FileSystem::Create(const fs::Path& file_path) {
  if (files_.contains(file_path.Repr())) {
    return fallible::Ok(false);
  }

  TIMBER_LOG_INFO("Create new file {}", file_path);
  auto f = CreateFile();
  files_.insert({file_path.Repr(), f});
  return fallible::Ok(true);
}

Status FileSystem::Unlink(const fs::Path& file_path) {
  TIMBER_LOG_INFO("Remove file {}", file_path);
  files_.erase(file_path.Repr());
  return fallible::Ok();
}

FileSystem::DirIterator FileSystem::ListAllFiles() {
  return {files_};
}

fs::FileList FileSystem::ListFiles(std::string_view prefix) {
  fs::FileList list;

  auto it = ListAllFiles();
  while (it.IsValid()) {
    if ((*it).starts_with(prefix)) {
      list.push_back(MakePath(*it));
    }
    ++it;
  }

  return list;
}

Status FileSystem::SyncDir(const fs::Path& /*dir_path*/) {
  return fallible::Ok();
}

fs::Path FileSystem::MakePath(std::string_view repr) {
  return fs::Path(*this, std::string(repr));
}

fs::Path FileSystem::RootPath() {
  return MakePath("/");
}

fs::Path FileSystem::TempPath() {
  return MakePath("/tmp");
}

std::string FileSystem::PathAppend(const std::string& base_path,
                                   const std::string& name) const {
  if (base_path.ends_with('/')) {
    return base_path + name;
  } else {
    return base_path + "/" + name;
  }
}

std::pair<std::string_view, std::string_view> FileSystem::PathSplit(const std::string& path) const {
  std::string_view path_view = path;

  size_t split_pos = path_view.find_last_of('/');
  if (split_pos != std::string::npos) {
    auto parent = path_view.substr(0, split_pos);
    auto name = path_view.substr(split_pos + 1, path_view.length());
    return {parent, name};
  } else {
    return {path, {}};
  }
}

FileSystem::FileRef FileSystem::FindOrCreateFile(const fs::Path& file_path,
                                                 FileMode open_mode) {
  auto it = files_.find(file_path.Repr());

  if (it != files_.end()) {
    return it->second;
  } else {
    // File does not exist
    if (open_mode == FileMode::Append) {
      TIMBER_LOG_INFO("Create file {}", file_path);
      auto f = CreateFile();
      files_.insert({file_path.Repr(), f});
      return f;
    } else {
      return {};
    }
  }
}

FileSystem::FileRef FileSystem::FindExistingFile(const fs::Path& file_path) {
  auto it = files_.find(file_path.Repr());
  WHEELS_ASSERT(it != files_.end(), "File " << file_path << " not found");
  return it->second;
}

FileSystem::FileRef FileSystem::CreateFile() {
  return std::make_shared<File>();
}

void FileSystem::CheckMode(OpenedFile& of, FileMode expected) {
  if (of.mode != expected) {
    RaiseError("Unexpected file mode");
  }
}

size_t FileSystem::InitOffset(FileRef f, FileMode open_mode) {
  switch (open_mode) {
    case FileMode::Read:
      return 0;
    case FileMode::Append:
      return f->Length();
  }
}

FileSystem::OpenedFile& FileSystem::GetOpenedFile(Fd fd) {
  auto it = opened_files_.find(fd);
  if (it == opened_files_.end()) {
    RaiseError("Fd not found");
  }
  return it->second;
}

void FileSystem::RaiseError(const std::string& message) {
  WHEELS_PANIC("Filesystem error: " << message);
}

// Simulation

void FileSystem::Corrupt(const fs::Path& file_path) {
  WHEELS_UNUSED(file_path);
  // TODO
}

size_t FileSystem::GetLength(const fs::Path& file_path) {
  return FindExistingFile(file_path)->Length();
}

fallible::Status FileSystem::Truncate(const fs::Path& file_path, size_t new_length) {
  auto f = FindExistingFile(file_path);
  TIMBER_LOG_INFO("Truncate file {} to {} bytes", file_path, new_length);
  f->Truncate(new_length);
  return fallible::Ok();
}

void FileSystem::Reset() {
  // Reset volatile state
  opened_files_.clear();
  next_fd_ = 0;
}

}  // namespace persist::fs::matrix
