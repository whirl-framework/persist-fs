#include <persist/log/header.hpp>

namespace persist::log {

const uint32_t kHeaderMagicNumber = 0xFEEEFEEE;

}  // namespace persist::log
