#include <persist/log/reader.hpp>

#include <persist/log/header.hpp>
#include <persist/log/checksum.hpp>

#include <rewrite/read/limit.hpp>
#include <rewrite/read/read.hpp>

#include <wheels/memory/view_of.hpp>

namespace persist::log {

LogReader::LogReader(const fs::Path& file_path)
    : file_reader_(file_path),
      buf_file_reader_(file_reader_) {
  Open();
}

void LogReader::Open() {
  done_ = file_reader_.Open().HasError();
}

bool LogReader::ReadNextPart(wheels::MutableMemView buffer) {
  size_t part_size = buffer.Size();

  rewrite::LimitReader part_reader(LogFileReader(), part_size);

  auto bytes_read = rewrite::Read(part_reader, buffer);
  return bytes_read.IsOk() && *bytes_read == part_size;
}

std::optional<Header> LogReader::ReadHeader() {
  Header header;

  if (!ReadNextPart(MutViewOf(header))) {
    return std::nullopt;
  }
  if (header.magic != kHeaderMagicNumber) {
    return std::nullopt;
  }
  return header;
}

std::optional<std::string> LogReader::ReadEntry(const Header& header) {
  std::string entry;
  entry.resize(header.size);
  if (!ReadNextPart(wheels::MutViewOf(entry))) {
    return std::nullopt;
  }
  if (ComputeChecksum(wheels::MutViewOf(entry)) != header.checksum) {
    return std::nullopt;
  }
  return entry;
}

std::optional<std::string> LogReader::ReadNext() {
  if (done_) {
    return std::nullopt;
  }

  // Read header
  auto header = ReadHeader();
  if (!header) {
    done_ = true;
    return std::nullopt;
  }

  // Read entry
  auto entry = ReadEntry(*header);
  if (!entry) {
    done_ = true;
    return std::nullopt;
  }

  // Advance offset
  offset_ += sizeof(Header) + entry->size();
  return entry;
}

}  // namespace persist::log
