#pragma once

#include <persist/fs/path.hpp>

#include <fallible/result/result.hpp>
#include <wheels/memory/view.hpp>

#include <string>
#include <vector>

namespace persist::fs {

///////////////////////////////////////////////////////////////////////

using Fd = int;

///////////////////////////////////////////////////////////////////////

enum class FileMode {
  Append,  // Do not format
  Read
};

///////////////////////////////////////////////////////////////////////

// Local file system (or distributed like Colossus?)

struct IFileSystem {
  virtual ~IFileSystem() = default;

  // Metadata

  // Creates new empty file and returns true
  // if file with path `file_path` does not exist yet,
  // returns false otherwise
  virtual fallible::Result<bool> Create(const Path& file_path) = 0;

  // Deletes a name from the filesystem
  // Missing file is Ok
  virtual fallible::Status Unlink(const Path& file_path) = 0;

  virtual bool Exists(const Path& file_path) const = 0;

  // ~ truncate
  virtual fallible::Status Truncate(const Path& file_path, size_t new_length) = 0;

  virtual FileList ListFiles(std::string_view prefix) = 0;

  // Persist directory changes
  virtual fallible::Status SyncDir(const Path& dir_path) = 0;

  // Data

  // FileMode::Append creates file if it does not exist
  virtual fallible::Result<Fd> Open(const Path& file_path, FileMode mode) = 0;

  // Only for FileMode::Append
  // Blocking
  virtual fallible::Status Append(Fd fd, wheels::ConstMemView data) = 0;

  // ~ pread
  // Only for FileMode::Read
  // Blocking
  virtual fallible::Result<size_t> Read(Fd fd, wheels::MutableMemView buffer) = 0;

  // ~ fsync
  virtual fallible::Status Sync(Fd fd) = 0;

  virtual fallible::Status Close(Fd fd) = 0;

  // Paths

  virtual Path MakePath(std::string_view repr) = 0;

  // Well-known paths

  virtual Path RootPath() = 0;
  virtual Path TempPath() = 0;

  // Internal methods used by Path

  // Appends `name` component to path `base`
  // Do not use directly, use path / "name" operator
  virtual std::string PathAppend(const std::string& base,
                                 const std::string& name) const = 0;

  // Split path to parent + name
  virtual std::pair<std::string_view, std::string_view> PathSplit(const std::string& repr) const = 0;

};

}  // namespace persist::fs
