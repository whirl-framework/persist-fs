#include <persist/log/checksum.hpp>

#include <crc32c/crc32c.h>

namespace persist::log {

uint64_t ComputeChecksum(wheels::ConstMemView data) {
  return crc32c::Crc32c(data.Begin(), data.Size());
}

}  // namespace persist::log
