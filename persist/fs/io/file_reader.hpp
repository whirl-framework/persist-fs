#pragma once

#include <persist/fs/fs.hpp>

#include <rewrite/reader.hpp>

#include <wheels/support/noncopyable.hpp>

namespace persist::fs {

//////////////////////////////////////////////////////////////////////

// Open -> [ReadSome] -> Close | ~FileReader
// TODO: Typestate correctness

class FileReader : public rewrite::IReader, private wheels::NonCopyable {
 public:
  FileReader(Path path);

  // Movable
  FileReader(FileReader&&);

  // Non-copyable
  FileReader(const FileReader&) = delete;

  ~FileReader();

  fallible::Status Open();

  // IReader
  fallible::Result<size_t> ReadSome(wheels::MutableMemView buffer) override;

  fallible::Status Close();

 private:
  bool IsValid() const;
  void EnsureValid();

  Fd ReleaseFd();

 private:
  IFileSystem& fs_;
  Path path_;
  Fd fd_;
};

//////////////////////////////////////////////////////////////////////

// Make and open new reader
fallible::Result<FileReader> Read(Path path);

}  // namespace persist::fs
