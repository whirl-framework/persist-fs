#include <persist/fs/io/file_reader.hpp>

#include <fallible/result/make.hpp>

using fallible::Ok;
using fallible::PropagateError;

namespace persist::fs {

//////////////////////////////////////////////////////////////////////

FileReader::FileReader(Path path)
    : fs_(path.Fs()), path_(std::move(path)) {
}

FileReader::FileReader(FileReader&& reader)
    : fs_(reader.fs_), path_(reader.path_),
      fd_(reader.ReleaseFd()) {
}

FileReader::~FileReader() {
  if (IsValid()) {
    Close().ExpectOk("Failed to close file");
  }
}

fallible::Status FileReader::Open() {
  auto fd = fs_.Open(path_, FileMode::Read);

  if (fd.IsOk()) {
    fd_ = *fd;
    return Ok();
  } else {
    fd_ = -1;
    return PropagateError(fd);
  }
}

fallible::Result<size_t> FileReader::ReadSome(wheels::MutableMemView buffer) {
  EnsureValid();
  return fs_.Read(fd_, buffer);
}

fallible::Status FileReader::Close() {
  EnsureValid();
  return fs_.Close(std::exchange(fd_, -1));
}

bool FileReader::IsValid() const {
  return fd_ != -1;
}

void FileReader::EnsureValid() {
  WHEELS_VERIFY(IsValid(), "Invalid state");
}

Fd FileReader::ReleaseFd() {
  return std::exchange(fd_, -1);
}

//////////////////////////////////////////////////////////////////////

fallible::Result<FileReader> Read(Path path) {
  FileReader reader(std::move(path));
  auto status = reader.Open();

  if (status.IsOk()) {
    return Ok(std::move(reader));
  } else {
    return PropagateError(status);
  }
}

}  // namespace persist::fs
