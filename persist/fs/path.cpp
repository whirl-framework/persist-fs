#include <persist/fs/path.hpp>
#include <persist/fs/fs.hpp>

#include <wheels/support/assert.hpp>

namespace persist::fs {

bool Path::IsRootPath() const {
  return repr_ == Fs().RootPath().Repr();
}

std::string Path::Name() const {
  auto [parent, name] = Fs().PathSplit(repr_);
  return std::string(name);
}

Path Path::Parent() const {
  IFileSystem& fs = Fs();
  auto [parent, name] = fs.PathSplit(repr_);
  return Path{fs, std::string{parent}};
}

Path& Path::operator/=(const std::string& name) {
  repr_ = Fs().PathAppend(repr_, name);
  return *this;
}

IFileSystem& Path::Fs() const {
  WHEELS_VERIFY(fs_ != nullptr, "Path is not related to filesystem");
  return *fs_;
}

}  // namespace persist::fs
