#pragma once

#include <wheels/memory/view.hpp>

namespace persist::log {

uint64_t ComputeChecksum(wheels::ConstMemView data);

}  // namespace persist::log
