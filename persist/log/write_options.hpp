#pragma once

namespace persist::log {

struct WriteOptions {
  WriteOptions() = default;

  // Sync file after append
  bool sync = true;
};

}  // namespace persist::log
