#pragma once

#include <persist/fs/fs.hpp>

#include <persist/matrix/fs/file.hpp>

#include <timber/log/logger.hpp>

#include <fallible/result/result.hpp>

#include <map>
#include <string>
#include <memory>
#include <vector>

namespace persist::fs::matrix {

//////////////////////////////////////////////////////////////////////

class FileSystem : public fs::IFileSystem {
  using FileRef = std::shared_ptr<File>;

  struct OpenedFile {
    fs::Fd fd;
    fs::Path path;
    fs::FileMode mode;
    size_t offset;
    FileRef file;
  };

  using Files = std::map<std::string, FileRef>;

  class DirIterator {
   public:
    DirIterator(Files& files) : it_(files.begin()), end_(files.end()) {
    }

    const std::string& operator*() {
      return it_->first;
    }

    bool IsValid() const {
      return it_ != end_;
    }

    void operator++() {
      ++it_;
    }

   private:
    Files::const_iterator it_;
    Files::const_iterator end_;
  };

 public:
  explicit FileSystem(timber::ILogBackend& log_backend);

  // System calls

  // Metadata

  fallible::Result<bool> Create(const fs::Path& file_path) override;
  fallible::Status Unlink(const fs::Path& file_path) override;
  bool Exists(const fs::Path& file_path) const override;

  fallible::Status Truncate(const fs::Path& file_path, size_t new_length) override;

  DirIterator ListAllFiles();

  fs::FileList ListFiles(std::string_view prefix) override;

  fallible::Status SyncDir(const fs::Path& dir_path) override;

  // Data

  fallible::Result<fs::Fd> Open(const fs::Path& file_path,
                              fs::FileMode mode) override;

  fallible::Result<size_t> Read(fs::Fd fd,
                              wheels::MutableMemView buffer) override;
  fallible::Status Append(fs::Fd fd, wheels::ConstMemView data) override;

  fallible::Status Sync(fs::Fd fd) override;

  fallible::Status Close(fs::Fd fd) override;

  // Paths

  fs::Path MakePath(std::string_view repr) override;

  fs::Path RootPath() override;

  fs::Path TempPath() override;

  std::string PathAppend(const std::string& base_path,
                         const std::string& name) const override;

  // Split path to parent + name
  std::pair<std::string_view, std::string_view> PathSplit(const std::string& path) const override;

  // Simulation

  // Fault injection
  void Corrupt(const fs::Path& file_path);

  size_t GetLength(const fs::Path& file_path);

  // On crash
  void Reset();

 private:
  FileRef FindOrCreateFile(const fs::Path& file_path, fs::FileMode open_mode);
  FileRef FindExistingFile(const fs::Path& file_path);

  static FileRef CreateFile();

  size_t InitOffset(FileRef f, fs::FileMode open_mode);

  void CheckMode(OpenedFile& of, fs::FileMode expected);

  OpenedFile& GetOpenedFile(fs::Fd fd);

  [[noreturn]] void RaiseError(const std::string& message);

 private:
  // Persistent state
  Files files_;

  // Process (volatile) state
  std::map<fs::Fd, OpenedFile> opened_files_;
  fs::Fd next_fd_{0};

  timber::Logger logger_;
};

}  // namespace persist::fs::matrix
