// Runtime
#include <persist/matrix/fs/fs.hpp>

#include <persist/fs/fs.hpp>

#include <persist/log/reader.hpp>
#include <persist/log/writer.hpp>

#include <timber/log/log.hpp>
#include <timber/log/backends/stdout/backend.hpp>

#include <wheels/memory/view_of.hpp>

#include <cassert>
#include <iostream>

using wheels::ViewOf;
using wheels::MutViewOf;

//////////////////////////////////////////////////////////////////////

// Runtime

timber::backends::StdoutLogBackend log_backend;
persist::fs::matrix::FileSystem fs(log_backend);

persist::fs::IFileSystem& Fs() {
  return fs;
}

persist::fs::matrix::FileSystem& FsImpl() {
  return fs;
}

timber::ILogBackend& LogBackend() {
  return log_backend;
}

//////////////////////////////////////////////////////////////////////

void CorruptLastEntryExample() {
  std::cout << "CorruptLastEntry example" << std::endl;

  auto log_path = Fs().TempPath() / "log.1";

  size_t entry_3_offset;

  {
    Fs().Create(log_path).ExpectOk();

    persist::log::LogWriter log_writer(log_path);

    log_writer.Open(0).ExpectOk();

    log_writer.Append(ViewOf("Entry1")).ExpectOk();
    log_writer.Append(ViewOf("Entry2")).ExpectOk();

    entry_3_offset = FsImpl().GetLength(log_path);

    log_writer.Append(ViewOf("Entry3")).ExpectOk();
  }

  // Corrupt last record
  FsImpl().Truncate(log_path, entry_3_offset + 5).ExpectOk();

  size_t writer_offset;

  {
    std::cout << "Read log " << log_path << ": " << std::endl;
    persist::log::LogReader log_reader(log_path);
    while (auto entry = log_reader.ReadNext()) {
      std::cout << "Entry: " << *entry << std::endl;
    }

    writer_offset = log_reader.OffsetForWriter();
  }

  {
    persist::log::LogWriter log_writer(log_path);
    log_writer.Open(writer_offset).ExpectOk();
    log_writer.Append(ViewOf("Entry4")).ExpectOk();
  }

  {
    std::cout << "Read log " << log_path << ": " << std::endl;
    persist::log::LogReader log_reader(log_path);
    while (auto entry = log_reader.ReadNext()) {
      std::cout << "Entry: " << *entry << std::endl;
    }
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void TruncateExample() {
  std::cout << "Truncate example" << std::endl;

  auto log_path = Fs().TempPath() / "log.2";

  Fs().Create(log_path).ExpectOk();
  Fs().Truncate(log_path, 1024).ExpectOk();

  persist::log::LogReader log_reader(log_path);
  if (log_reader.ReadNext()) {
    std::abort();
  } else {
    std::cout << "No entries!" << std::endl;
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  CorruptLastEntryExample();
  TruncateExample();
  return 0;
}
